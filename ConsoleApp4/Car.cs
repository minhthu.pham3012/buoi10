﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    public class Car
    {
        public string Color { get; set; }
        public string Brand { get; set; }
        public int Year { get; set; }
        public double Price { get; set; }

        public void DisplayInfo()
        {
            Console.WriteLine($"Brand: {Brand}, Color: {Color}, Year: {Year}, Price: ${Price}");
        }

        public double CalculateResidualValue(int years, double rate)
        {
            double residualValue = Price;
            for (int i = 1; i <= years; i++)
            {
                residualValue -= residualValue * (rate / 100);
            }
            return residualValue;
        }
    }
}
