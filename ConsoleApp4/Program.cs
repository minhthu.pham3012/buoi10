﻿//using ConsoleApp4;

//List<Nhanvien> danhSachNhanVien = new List<Nhanvien>
//        {
//            new Nhanvien("NV001", "John Doe", 30, "Dev"),
//            new Nhanvien("NV002", "John Do", 25, "Dev"),
//            new Nhanvien("NV003", "John Do", 35, "Dev"),
//            new Nhanvien("NV004", "John Do", 28, "Dev"),
//            new Nhanvien("NV005", "John Do", 40, "Dev")
//        };

//Console.WriteLine("Thông tin của 5 nv :");
//for (int i = 0; i < 5 && i < danhSachNhanVien.Count; i++)
//{
//    Nhanvien nhanvien = danhSachNhanVien[i];
//    Console.WriteLine($"Mã nhân viên: {nhanvien.MaNhanVien}, Tên: {nhanvien.Ten}, Tuổi: {nhanvien.Tuoi}, Chức vụ: {nhanvien.ChucVu}");
//}



//A a = new A(); // khởi tạo đối tượng 
//a.arr = new int[] { 1, 2, 3, 4 }; // khởi tạo mảng và gán vào thuộc tính arr
//Console.WriteLine(a.arr[0]);

//class A
//{
//    public int[] arr { get; set; }
//}



// Khởi tạo đối tượng Person
//Person person = new Person();

//// In ra các giá trị của các thuộc tính
//Console.WriteLine($"Name: {person.Name}");
//Console.WriteLine($"Age: {person.Age}");
//Console.WriteLine("Emails:");

//foreach (string email in person.Emails)
//{
//    Console.WriteLine(email);
//}

//class Person
//{
//    public string Name { get; set; } = "John";
//    public int Age { get; set; } = 30;
//    public string[] Emails { get; set; } = new string[] { "abc@gmail.com", "def@gmail.com" };
//}

//A a = new A
//{
//    a = "Tom",
//};

//Console.WriteLine(a.a);
//Console.WriteLine(a.b.z);

//class A
//{
//    public string a { get; set; }
//    public B b { get; set; } // thuộc tính b có kiểu là B
//}

//class B
//{
//    public int z { get; set; }
//}

//Student a = new Student()
//{
//    name= "Thu",
//    age = 19,
//};
//a = new Student();
//Console.WriteLine(a);

//class Student
//{
//    public string name { get; set; }
//    public int age { get; set; }

//    public void Detail()
//    {
//        Console.WriteLine($"Name: {name}, Age: {age}");
//    }
//}


//A a = new A();
//a.a = "xin chao";
//a.X();

//A b = new A();
//b.a = "hello b";
//b.X();

//Console.WriteLine(ReferenceEquals(a, b));

//class A
//{
//    public string a { get; set; }
//    public void X()
//    {
//        Console.WriteLine(a);
//    }
//}





using ConsoleApp4;

Car myCar = new Car
{
    Color = "White",
    Brand = "Vinfast",
    Year = 2022,
    Price = 25000
};

Console.WriteLine("Car Information:");
myCar.DisplayInfo();

int years = 3;
double rate = 10;
double residualValue = myCar.CalculateResidualValue(years, rate);
Console.WriteLine($"Residual value after {years} years: ${residualValue}");




