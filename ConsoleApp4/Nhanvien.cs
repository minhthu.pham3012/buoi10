﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    public  class Nhanvien
    {
        public string MaNhanVien { get; set; }
        public string Ten { get; set; }
        public int Tuoi { get; set; }
        public string ChucVu { get; set; }

        public Nhanvien(string maNhanVien, string ten, int tuoi, string chucVu)
        {
            MaNhanVien = maNhanVien;
            Ten = ten;
            Tuoi = tuoi;
            ChucVu = chucVu;
        }
    }
   

}


